// Julio Aguilar 03/01/2024
// The state machine sourcecode was not provided as an ino file on the project, 
// It was only listed as a list of screenshots!

#include <EMGFilters.h>

#include "Arduino.h"

#define SensorInputPin 4 // input pin number
EMGFilters myFilter;
SAMPLE_FREQUENCY sampleRate = SAMPLE_FREQ_1000HZ;
NOTCH_FREQUENCY humFreq = NOTCH_FREQ_50HZ;
static int Threshold =9;

unsigned long timeStamp;
unsigned long timeBudget;
long datagame=0;
int state =0;
int pulse_count=0;
int time_count=0;

void setup() {
    myFilter.init(sampleRate, humFreq, true, true, true);
    Serial.begin(115200); // open serial
    // setup for time cost measure using micros()
    timeBudget = 1e6 / sampleRate;
    // micros will overflow and auto return to zero every 70 minutes
    pinMode(A0,INPUT);
}
unsigned long t0;
unsigned long t1;
int autom = 0;

void loop() {
    timeStamp=micros();
    int Value = analogRead(SensorInputPin);
    // filter processing
    int DataAfterFilter = myFilter.update(Value);
    int envlope=sqrt(sq((DataAfterFilter));
    // any value under threshold will be set to zero
    envlope = (envlope > Threshold) ? envlope : 0;

    timeStamp = micros() - timeStamp;
	t0=micros();
	if ((t0-t1)>=500000) {
		t1=t0;
		Serial.println(datagame);
		switch(state) {
			case 0:
             if (envlope){
				 state=1;
			 }
			 break;
			case 1:
             if (!envlope){
				 pulse_count++;
				 time_count=0;
				 state=2;
			 }
			 break;
			case 2:
			time_count++;
             if (envlope){
				 state=1;
			 }
             if (time_count>10){				 if (pulse_count==1) state=3;

				 if (pulse_count==1) state=3;
				 if (pulse_count==2) state=4;
				 if (pulse_count>2) state=5;
			 }
			 break;

			case 3:
			datagame=1;
             if (!envlope){
				 state=0;
				 pulse_count=0;
			 }
			 break;
			case 4:
			datagame=1;
			if (!envlope){
				 state=0;
				 pulse_count=0;
			 }
			 break;
			case 5:
				 state=0;
pulse_count=0; 
state=0;
			 break;
}
}
	delayMicroseconds(500);	
   
}
