#include <EMGFilters.h>

#include "Arduino.h"

#define SensorInputPin 4 // input pin number
#define MAX_OPERATION 3
EMGFilters myFilter;
SAMPLE_FREQUENCY sampleRate = SAMPLE_FREQ_1000HZ;
NOTCH_FREQUENCY humFreq = NOTCH_FREQ_50HZ;
static int Threshold =320;

unsigned long timeStamp;
unsigned long timeBudget;
void setup() {
    /* add setup code here */
    myFilter.init(sampleRate, humFreq, true, true, true);

    // open serial
    Serial.begin(115200);

    // setup for time cost measure
    // using micros()
    timeBudget = 1e6 / sampleRate;
    // micros will overflow and auto return to zero every 70 minutes
    pinMode(SensorInputPin,INPUT);
}
unsigned long t0;
unsigned long t1;
int autom = 0;
void loop() {
    /* add main program code here */
    // In order to make sure the ADC sample frequence on arduino,
    // the time cost should be measured each loop
    /*------------start here-------------------*/
    timeStamp = micros();

    int Value = analogRead(SensorInputPin);

    // filter processing
    int DataAfterFilter = myFilter.update(Value);

    int envlope=abs(DataAfterFilter);

    // any value under threshold will be set to zero
    envlope = (envlope > Threshold) ? envlope : 0;

              
   //t0 = micros();
       Serial.println(envlope);  
     
}
