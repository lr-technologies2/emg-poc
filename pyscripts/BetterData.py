import pandas as pd
import xlsxwriter
import DataConditionningFunc as dcf

data = pd.read_excel("RawData.xlsx", engine='openpyxl') # data set des différents mouvements
finger = list(data['Finger'].values) #data set du mouvement de l'annulaire fléchie
lowered = list(data['Lowered'].values)#data set du mouvement de la main fléchie
clenched = list(data['Clenched'].values)#data set du mouvement du poing serré

"""
Finger_zcount =[]
Lowered_zcount = []
Clenched_zcount = []
workbook = xlsxwriter.Workbook("ZeroCountFC.xlsx")
dcf.ZeroCount(finger,Finger_zcount,1,workbook)
dcf.ZeroCount(clenched,Clenched_zcount,2,workbook)
dcf.ZeroCount(lowered,Lowered_zcount,3,workbook)
workbook.close()

"""

#Les séquences maximales de zéros acceptées dans les données (ZeroCount de DataconditionningFunc)
Max_Zero_Finger = 80
Max_Zero_Clenched = 46
Max_Zero_Lowered = 31



dcf.Zerodel(finger,Max_Zero_Finger)
dcf.Zerodel(clenched,Max_Zero_Clenched)
dcf.Zerodel(lowered,Max_Zero_Lowered)





pr_finger=[]
pr_clenched=[]
pr_lowered =[]

dcf.feats_extract(finger,pr_finger) # Extraction des features du mouvement de l'annulaire fléchie
dcf.feats_extract(clenched,pr_clenched)# Extraction des features du mouvement du poing serré
dcf.feats_extract(lowered,pr_lowered)# Extraction des features du mouvement de la main fléchie

print(len(pr_clenched))
print(len(pr_finger))
print(len(pr_lowered))
Min = min([len(pr_clenched),len(pr_lowered),len(pr_finger)])
Finaldata = [pr_clenched,pr_lowered,pr_finger]
Header = ["Type","IEMG","MEAN","VAR","MMAV","WL","SKEW","KURT"]
workbook2 = xlsxwriter.Workbook("Data1.xlsx") # Data1 est l'excel dans lequel les échantillons vont être mis
worksheet = workbook2.add_worksheet("F1")
for z,headline in enumerate(Header):
    worksheet.write(0,z,headline)
for i in range(len(Finaldata)):
    for x in range(len(Finaldata[i])):
        for y,type in enumerate(Finaldata[i][x]):
            worksheet.write(x+1+(i*Min),0,i)
            worksheet.write(x+1+(i*Min),y+1,type)
workbook2.close()