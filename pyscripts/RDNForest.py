import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 

data = pd.read_excel("Data1.xlsx", engine='openpyxl')
print(data.head())

from sklearn.model_selection import train_test_split
X_train,X_test, y_train,y_test= train_test_split(data.drop(['Type'],axis= 1),data['Type'],test_size=0.2)

from sklearn.ensemble import RandomForestClassifier
model=RandomForestClassifier(n_estimators=100)
model.fit(X_train,y_train)
print(model.score(X_test,y_test))

y_predicted= model.predict(X_test)
from sklearn.metrics import confusion_matrix
cm  = confusion_matrix(y_test,y_predicted)

import matplotlib.pyplot as plt 
import seaborn as sns
plt.figure(figsize=(10,7))
sns.heatmap(cm,annot=True)
plt.xlabel('Predicted')
plt.ylabel('Truth')
plt.show()

