import serial
import time
import numpy as np
import DataConditionningFunc as dcf

def Test(Data,model): #### Fonction de test de classification d'un échantillon (une des frames de 500 points) offline
    x= np.random.randint(0,len(Data)) # index de l'échantillon aléatoire
    z=Data.iloc[x] #L'échantillon aléatoire
    y=np.array(list(Data.drop(['Type'],axis=1).iloc[x]))
    print("Truth =",z[0]) # type de mouvement
    print(model.predict(y.reshape(1,5))) # prédiction du type de mouvement (il faut reshape selon le nombre de features, ici 5)

def movement(model) : #Fonction pour qui capte un mouvement sur le port série et le classifie selon le model
    ser = serial.Serial("COM3", 115200)
    time.sleep(1)
    ser.flushInput()
    temp= np.array([])
    final = np.array([])
    flag = False
    while True :
        try:
            Data_string=ser.readline()
            Data=float(Data_string)
            print(Data)
            if (Data > 0):
                temp=np.append(temp,Data)
                break
        except:
            pass  
    #########                                                                                                              ############
    for i in range (499): ### Frame de 500 points
        try:
            Data_string=ser.readline()
            Data=float(Data_string)
            print(Data)
            temp=np.append(temp,Data) ### récupération des 500 points dans une liste temporaire
        except:
            pass
    ###########                                                  Pré-processing de la frame de 500 points                              ############
    final=np.append(final,np.sum(temp))
    final=np.append(final,np.mean(temp))
    final=np.append(final,np.var(temp))
    final=np.append(final,dcf.MMAV(temp))
    final=np.append(final,dcf.WL(temp))
    ###########                                                                                                                        ############
    print(final)
    print(model.predict(final.reshape(1,5)))