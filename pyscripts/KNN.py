import pandas as pd
import Tests 


###########                                                  Création du modèle KNN                                                 ############

data = pd.read_excel("Data1.xlsx", engine='openpyxl')
from sklearn.neighbors import KNeighborsClassifier
model = KNeighborsClassifier()
y = data['Type'] ### type de mouvemenent ( 0 : Poing serré , 1 : Main fléchie, 3 : Annulaire fléchie)
x = data.drop('Type', axis= 1 ) ### features de classification (MEAN,VAR,MMAV,WL)
model.fit(x,y)
print(model.score(x,y))  #### fiabilité du model
 
###########                                                   Fonctions de test online et offline                              ############

#Tests.Test(data,model)
#Tests.movement(model)