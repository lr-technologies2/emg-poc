import numpy as np
def ZeroCount(Data,Tabcount,Sheet,Workbook):
    """Renvoie la taille des différentes suites de 0 et l'écrit dans un excel"""
    count = 0
    flag = True
    for i in Data :
        if (flag==True):
            if (i==0):
                count+=1
            else :
                flag = False
                Tabcount.append(count)
                count =0
        else : 
            if(i==0):
                count+=1
                flag = True
            else :
                flag =False
    
    worksheet = Workbook.add_worksheet(f"F{Sheet}")
    for i,count in enumerate(Tabcount):
        worksheet.write(i,0,count) 

def Zerodel(Rawdata,MaxCount):
    """Supprime les suites de 0 de taille supérieur au maximum spécifié"""
    i=0
    z_index=0
    flag=False
    while (i<len(Rawdata)) :
        if (flag==False):
            if (Rawdata[i]==0):
                z_index = i
                flag=True
                i+=1
            else :
                i+=1
        else : 
            if ((Rawdata[i]==0) & (i!=len(Rawdata)-1)):
                i+=1
            elif(i==len(Rawdata)-1):
                if((i-z_index)>MaxCount):
                    del Rawdata[z_index:i+1]

            elif(Rawdata!=0) :
                flag = False
                if((i-z_index)>MaxCount):
                    del Rawdata[z_index:i]
                i=i+1
                
def MMAV(array):
   """Calcul de la moyenne pondéré"""
   size = len(array)  
   sum=0
   for i in range(size):
       if i>= int(0.25*size) & i<= int(0.75*size) :
           sum+= array[i]
       else :
           sum+=0.5*array[i]
   return sum/size

def WL(array):
    """(Waveform Length) Mesure de la somme des différences absolues entre des échantillons consécutifs"""
    size = len(array)
    sum=0
    for i in range(size-1) :
        sum+=abs(array[i+1]-array[i])
    return sum

def feats_extract(rawdata,pdata): #pdata pour processed data
    """Extraction des features de classification sur des segments de 500 points du dataset de base"""
    i = 0
    temp = [] # Temp contient les features d'un échantillon à la fois
    while i<len(rawdata): # Mise dans un tableau pdata les features de chacun des échantillons pris par frame de 500 points
                if(np.sum(rawdata[i:i+500])!=0): #On récupère uniquement les échantillons non nuls
                    temp.append(np.sum(rawdata[i:i+500]))
                    temp.append(np.mean(rawdata[i:i+500]))
                    temp.append(np.var(rawdata[i:i+500]))
                    temp.append(MMAV(rawdata[i:i+500]))
                    temp.append(WL(rawdata[i:i+500]))
                    pdata.append(temp) 
                    i=i+500
                else:
                     i=i+500
                temp = []