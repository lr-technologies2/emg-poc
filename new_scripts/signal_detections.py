import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os 
from collections import deque
import time
import matplotlib.animation as animation
from scipy.signal import savgol_filter

def csv2array(filePath):
    name = os.path.basename(filePath)
    df = pd.read_csv(filePath)
    sign_b = np.array(df['Value1'].values)
    sign_f1 = np.array(df['Value2'].values)
    sign_f2 = np.array(df['Value3'].values)

    timeStep = 0.001 #### point récupéré toutes les millisecondes 
    n = sign_b.shape
    timeValues = np.arange(0,n[0]*timeStep,timeStep)

    return sign_b, sign_f1, sign_f2, timeValues



def variance_signal_detection(signal, timeStep=100, epsilonVariance=0.1):

    signalDetection = []

    i = 0
    while i < len(signal):
        variance = np.var(signal[i:i+timeStep])
        if variance <= epsilonVariance:
            i += timeStep
            continue
        else:
            start = i
            while i < len(signal) and np.var(signal[i:i+timeStep]) > epsilonVariance:
                i += timeStep
            stop = i + timeStep
            if np.var(signal[start:stop]) > epsilonVariance:
                signalDetection += [[start, stop]]

    return np.array(signalDetection)


def show_signal_detection(arrIndex, x, y, y2=None):

    count = 0
    for index in arrIndex:
        print(f'================ signal {count}  | variance {np.var(y[index[0]:index[1]])} ===============')
        plt.figure()
        plt.subplot(131)
        if y2.any() != None:
            plt.plot(x[index[0]:index[1]],y2[index[0]:index[1]], label="signal brute")
        plt.plot(x[index[0]:index[1]],y[index[0]:index[1]], label ="signal detected")
        plt.legend()
        plt.xlabel("Temps")
        plt.ylabel("Amplitude")
        plt.grid()

        plt.subplot(132)
        startplot = max(index[0] - 900, 0)
        endplot = min(index[1] + 900, x.shape[0] - 1)
        if y2.any() != None:
            plt.plot(x[startplot:endplot],y2[startplot:endplot], label="signal brute")
        plt.plot(x[startplot:endplot],y[startplot:endplot], label ="signal")
        plt.axvline(x=x[index[0]], c='r')
        plt.axvline(x=x[min(index[1],endplot)],c='r')
        plt.legend()
        plt.xlabel("Temps")
        plt.ylabel("Amplitude")
        plt.grid()

        plt.subplot(133)
        startplot = max(index[0] - 2000, 0)
        endplot = min(index[1] + 2000, x.shape[0] - 1)
        if y2.any() != None:
            plt.plot(x[startplot:endplot],y2[startplot:endplot], label="signal brute")
        plt.plot(x[startplot:endplot],y[startplot:endplot], label ="signal")
        plt.axvline(x=x[index[0]], c='r')
        plt.axvline(x=x[min(index[1],endplot)],c='r')
        plt.legend()
        plt.xlabel("Temps")
        plt.ylabel("Amplitude")
        plt.grid()
        plt.show()

        count += 1


def main():
    ### Signal one
    sign_b1, sign_f1, sign_fl1, timeValues1 = csv2array(r'../test/data/_out_with_majeur_move2.csv')
    ### Signal two
    sign_b2, sign_f2, sign_fl2, timeValues2 = csv2array(r'../test/data/_out_with_move_poignet2.csv')
    ### Signal three
    sign_b3, sign_f3, sign_fl3, timeValues3 = csv2array(r'../test/data/_out_with_move_poigt.csv')



    ### PROCESSING SIGNAL ONE
    savgol_sig1 = savgol_filter(sign_fl1, 300, 2)
    sigDetec = variance_signal_detection(savgol_sig1,200)
    show_signal_detection(sigDetec, timeValues1, savgol_sig1)


    ### PROCESSING SIGNAL TWO
    savgol_sig2 = savgol_filter(sign_fl2, 300, 2)
    sigDetec2 = variance_signal_detection(savgol_sig2,230, 0.8)
    show_signal_detection(sigDetec2, timeValues2, savgol_sig2)
    # SIGNAL TWO RETRIEVING NOISE
    noise_2 = sign_fl2[5000:6000]
    noiseExtended_fl2 = np.array([noise_2] * int(np.ceil(timeValues2.shape[0]/noise_2.shape[0])))
    noiseExtended_fl2 = noiseExtended_fl2.reshape(-1)
    noiseExtended_fl2 = noiseExtended_fl2[:timeValues2.shape[0]]

    sign_fl2_zero = sign_fl2 - noiseExtended_fl2
    savgol_sig2_zero = savgol_filter(sign_fl2_zero, 300, 2)
    func = lambda x: 0 if x < 0 else x
    savgol_sig2_zero = [func(x) for x in savgol_sig2_zero]
    sigDetec2 = variance_signal_detection(savgol_sig2_zero,230, 1.0)
    show_signal_detection(sigDetec2, timeValues2, savgol_sig2_zero)


    ### PROCESSING SIGNAL THREE
    savgol_sig3 = savgol_filter(sign_fl3, 300, 2)
    sigDetec3 = variance_signal_detection(savgol_sig3, 200, 0.5)
    show_signal_detection(sigDetec3, timeValues3, savgol_sig3)



if __name__ == '__main__':
    main()
