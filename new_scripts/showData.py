import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os 
from collections import deque
import time
import matplotlib.animation as animation

def csv2array(filePath):
    name = os.path.basename(filePath)
    df = pd.read_csv(filePath)
    sign_b = np.array(df['Value1'].values)
    sign_f1 = np.array(df['Value2'].values)
    sign_f2 = np.array(df['Value3'].values)

    timeStep = 0.001 #### point récupéré toutes les millisecondes 
    n = sign_b.shape
    timeValues = np.arange(0,n[0]*timeStep,timeStep)

    return sign_b, sign_f1, sign_f2, timeValues

def visualisation (filePath,):
    name = os.path.basename(filePath)
    df = pd.read_csv(filePath)
    sign_b = np.array(df['Value1'].values)
    sign_f1 = np.array(df['Value2'].values)
    sign_f2 = np.array(df['Value3'].values)

    timeStep = 0.001 #### point récupéré toutes les millisecondes 
    n = sign_b.shape
    timeValues = np.arange(0,n[0]*timeStep,timeStep)
    # print (timeValues)

    plt.figure()
    plt.suptitle(name)
    plt.subplot(211)
    plt.plot(timeValues,sign_b, label ="Signal Brute")
    plt.legend()
    plt.xlabel("Temps")
    plt.ylabel("Amplitude")
    plt.grid()

    plt.subplot(212)
    plt.plot(timeValues,sign_f1, label ="Signal filtré")
    plt.plot(timeValues,sign_f2, label ="Signal lissé")
    # plt.plot(timeValues,np.log10(sign_f2), label ="log du Signal Lisse")
    plt.legend()
    plt.xlabel("Temps")
    plt.ylabel("Amplitude")
    plt.grid()
    plt.show()

def signal_detection(sign_f1, sign_lisse,timeValues):
    length = sign_f1.shape[0]
    len2compare = 1000 
    d_val = list(sign_f1[:len2compare])
    d_liss = list(sign_lisse[:len2compare])
    d_time = list(timeValues[:len2compare])
    n = 1000 
    while n < length :
        diff = d_liss[-1]-d_liss[0]
        if diff > 30 :
            start = n-100
            end = n+900
            d_val = list(sign_f1[start:end])
            d_time = list(timeValues[start:end])
            d_liss = list(sign_lisse[start:end])
            plt.figure()
            plt.plot(d_time,d_val, label ="premier filtrage")
            plt.plot(d_time,d_liss, label ="signal lisse")
            plt.legend()
            plt.xlabel("Temps")
            plt.ylabel("Amplitude")
            plt.grid()
            plt.show()
            n = end
            plt.figure()
            plt.plot(timeValues,sign_f1, label ="Signal filtré")
            plt.plot(timeValues,sign_f2, label ="Signal lissé")
            plt.axvline(x=timeValues[start], c='r')
            plt.axvline(x=timeValues[end],c='r')
            plt.legend()
            plt.xlabel("Temps")
            plt.ylabel("Amplitude")
            plt.grid()
            plt.show()
        else :
            d_val.append(sign_f1[n])
            del(d_val[0])
            d_time.append(timeValues[n])
            del(d_time[0])
            d_liss.append(sign_lisse[n])
            del(d_liss[0])
            n = n + 1
        # print(n)

# def signal_visu(sign_f1, sign_lisse,timeValues):
#     length = sign_f1.shape[0]
#     len2compare = 1000 
#     d_val = deque(sign_f1[:len2compare])
#     d_time = deque(timeValues[:len2compare])
#     n = 1000 
#     plt.ion()  # Mode interactif
#     fig = plt.figure()
#     line, = plt.plot([], [])
#     plt.legend()
#     plt.xlabel('Temps (s)')
#     plt.ylabel('Valeur')
#     plt.title('visualisation en fenêtre glissante')
#     while n < length :
#     # for elt in sign_f1[len2compare:]:  
#         line.set_data(d_time, d_val)      
#         # plt.plot(d_time,d_val,label = "signal")
#         d_val.append(sign_f1[n])
#         d_val.popleft()
#         d_time.append(timeValues[n])
#         d_time.popleft()

#         fig.canvas.draw()
#         time.sleep(0.001)
#     plt.close(fig)

            
if __name__ == "__main__": 
    
    database_path = "C:\_Work\EMG\emg-poc\\new_dataset\\"
    dataFiles = os.listdir(database_path)
    print(dataFiles)
    # for file in dataFiles :
    #     filePath = database_path + file
    #     visualisation(filePath)

    filePath = database_path+dataFiles[0]
    
    visualisation(filePath)
    sign_b, sign_f1, sign_f2, timeValues = csv2array(filePath)
    signal_detection(sign_f1, sign_f2, timeValues)




