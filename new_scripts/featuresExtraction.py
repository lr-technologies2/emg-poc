import numpy as np
from math import *
import matplotlib.pyplot as plt

#### Integrated EMG
def IEMG(signal):           
    return np.sum(np.absolute(signal))

#### Mean Absolute Value
def MAV(signal):
    return np.mean(np.absolute(signal))

#### Variance
def VAR(signal):
    return np.var(signal)

#### Waveform Length
def WL(signal):
    n =  len(signal)
    wl = 0
    for i in range(1,n):
        wl = wl + abs(signal[i]-signal[i-1])
    return wl

#### Mean Absolute Value Slope
def MAVS(signal, pastSignal):
    return(MAV(signal)-MAV(pastSignal))

#### ZeroCrossing
def ZC(signal, threshold=0):
    n = len(signal)
    count = 0
    for i in range(1,n):
        if (signal[i]*signal[i-1]) < 0 and abs(signal[i]-signal[i-1])>threshold :
            count = count + 1
    return count

#### Root Mean Sqaure
def RMS(signal):
    n = len(signal)
    rms = 0
    for i in range(n):
        rms = rms + signal[i]**2
    rms = rms/n
    return sqrt(rms)

#### Modified Mean Absolute Value
def MMAV(signal):
    n = len(signal)
    mmav = 0
    weight = 0
    for i in range(n):
        if (i<=int(0.25*n)) or (i>=int(0.75*n)):
            weight = 1
        else :
            weight = 0.5
        mmav = mmav + weight*abs(signal[i]) 
    return mmav/n


if __name__ == "__main__": 
    # signal = np.random.uniform(low=-5, high=5, size=10000).tolist()
       
    # simulate EMG signal
    burst1 = np.random.uniform(-1, 1, size=1000) + 0.08
    burst2 = np.random.uniform(-1, 1, size=1000) + 0.08
    quiet = np.random.uniform(-0.05, 0.05, size=500) + 0.08
    emg = np.concatenate([quiet, burst1, quiet, burst2, quiet])
    time = np.array([i/1000 for i in range(0, len(emg), 1)]) # sampling rate 1000 Hz

    # plot EMG signal
    fig = plt.figure()
    plt.plot(time, emg)
    plt.xlabel('Time (sec)')
    plt.ylabel('EMG (a.u.)')
    fig_name = 'fig2.png'
    fig.set_size_inches(w=11,h=7)
    plt.show()
    # fig.savefig(fig_name)
    
    iemg = list()
    mav = list()
    var = list()
    wl = list()
    zc = list()
    rms = list()
    mmav = list()

    signal_size = len(emg)
    segment_size = 20 
    n = signal_size/segment_size

    for i in range(int(n)):
        segment = emg[i*segment_size:i*segment_size+segment_size]
        iemg.append(IEMG(segment))
        mav.append(MAV(segment))
        var.append(VAR(segment))
        wl.append(WL(segment))
        zc.append(ZC(segment))
        rms.append(RMS(segment))
        mmav.append(MMAV(segment))










    