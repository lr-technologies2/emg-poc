import featuresExtraction as fe 
import numpy as np 
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from joblib import dump, load
from sklearn import svm


##### Lecture des données + extraction de features à partir d'un fichier

data = pd.read_excel("C:\\_Work\\EMG\\emg-poc\\useddataset\\RawData.xlsx", engine='openpyxl')
mvt1 = list(data['Finger'].values)
mvt2 = list(data['Lowered'].values)
mvt3 = list(data['Clenched'].values)
timeValues = list(data['Time'].values)
signalList = [mvt1,mvt2,mvt3]

window_size = 500
overlap = 100 

iemg = list()
mav = list()
var = list()
wl = list()
zc = list()
rms = list()
mmav = list()
classification = list() 

for i in range(len(signalList)):
    signal = signalList[i]
    classe = i

    begin = 0
    end =  begin + window_size
    while end < len(signal): 
        segment = signal[begin:end]
        iemg.append(fe.IEMG(segment))
        mav.append(fe.MAV(segment))
        var.append(fe.VAR(segment))
        wl.append(fe.WL(segment))
        zc.append(fe.ZC(segment))
        rms.append(fe.RMS(segment))
        mmav.append(fe.MMAV(segment))
        classification.append(classe)


        begin = begin + (window_size - overlap)
        end =  begin + window_size

######## entraînement d'un modèle 
        
X = np.array([iemg, mav, var ,wl, zc, rms, mmav])
print(X.shape)
X = X.reshape(X.shape[1],X.shape[0])
print(X.shape)
Y = np.array(classification)
print(Y)

X_train,X_test, Y_train,Y_test= train_test_split(X,Y, test_size=0.2, shuffle=True)
print(Y_test)

#### model KNN
model_knn = KNeighborsClassifier()
model_knn.fit(X_train,Y_train)
print("model knn: ",model_knn.score(X_test,Y_test))

#### model SVM
model_svm = svm.SVC()
model_svm.fit(X_train, Y_train)
print("model svm: ",model_svm.score(X_test,Y_test))

### sauvegarde du model
dump(model_knn, 'knn.joblib')

### chargement du model =  verification qu'il est identique 
loaded_model = load('knn.joblib')
print("model_loaded: ",loaded_model.score(X_test,Y_test))










